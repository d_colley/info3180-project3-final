# coding=utf-8;

from flask_wtf import Form
from wtforms import StringField, IntegerField, FileField, SelectField
from wtforms.validators import InputRequired, Length, ValidationError

SEX = {'0': 'male','1': 'female'}
SEX_CHOICES = list((k, v) for k, v in SEX.iteritems())

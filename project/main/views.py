# project/main/views.py


#################
#### imports ####
#################

from flask import render_template, Blueprint
from flask.ext.login import login_required


################
#### config ####
################

main_blueprint = Blueprint('main', __name__,)


################
#### routes ####
################

@main_blueprint.route('/')
@login_required
def home():
    return render_template('main/index.html')

@main_blueprint.route('/platformgame')
def game1():
    return render_template('main/platformer/index.html')

@main_blueprint.route('/spacegame')
def game2():
    return render_template('main/spaceinvader/index.html')

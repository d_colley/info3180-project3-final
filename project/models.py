# project/models.py


import datetime
from time import time
from main.forms import SEX
from project import db, bcrypt
from flask import jsonify


class User(db.Model):

    __tablename__ = "users"

    # USER CREDENTIAL + LOGIN RELATED FIELDS
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    confirmed_on = db.Column(db.DateTime, nullable=True)
    # OTHER USER FIELDS
    username = db.Column(db.String(80), unique=True, nullable=True)
    first_name = db.Column(db.String(80),unique=False, nullable=True)
    last_name = db.Column(db.String(80), unique=False, nullable=True)
    age = db.Column(db.String(80), unique=False, nullable=True)
    sex = db.Column(db.String(1), nullable=True)
    image = db.Column(db.String(100), nullable=True)
    high_score = db.Column(db.Integer, nullable=True, default=0)
    tdollars = db.Column(db.Integer, nullable=True, default=0)

    def __init__(self, email, password, username, first_name, last_name, age, confirmed, sex=0, confirmed_on=None, admin=False):
        self.email = email
        self.password = bcrypt.generate_password_hash(password)
        self.registered_on = datetime.datetime.now()
        self.admin = admin
        self.confirmed = confirmed
        self.confirmed_on = confirmed_on
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.sex = sex
        # self.id = User.generate_user_id()

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def get_sex_display(self):
        return SEX[self.sex]

    @staticmethod
    def generate_user_id():
        new_id = time()
        new_id = str(long(new_id))
        return long(new_id)

    def __repr__(self):
        return 'Email:{}, Username:{}, First Name:{}, Last Name:{}'.format(self.email,self.username,self.first_name,self.last_name)

    def get_image_url(self):
        return '/uploads/{0}'.format(self.image)

    def to_json(self):
        return json.dumps({
            "id": self.id,
            "email":self.email,
            "username": self.username,
            "first_name":self.first_name,
            "last_name":self.last_name,
            "image": "...",
            "sex": self.get_sex_display(),
            "age": self.age,
            "profile_registered_on": self.registered_on.strftime("%Y-%m-%d"),
            "high_score": self.high_score,
            "tdollars": self.tdollars,
            })

    def get_sex_display(self):
        return SEX[self.sex]

# project/user/forms.py


from flask_wtf import Form
from wtforms import TextField, PasswordField, IntegerField, SelectField
from wtforms.validators import DataRequired, InputRequired, Email, Length, EqualTo
from project.models import User

SEX = {'0': 'Male','1': 'Female'}
SEX_CHOICES = list((k, v) for k, v in SEX.iteritems())

class LoginForm(Form):
    email = TextField('email', validators=[DataRequired(), Email()])
    password = PasswordField('password', validators=[DataRequired()])


class SignupForm(Form):
    username = TextField('First Name',validators=[DataRequired(), Length(max=80)])
    first_name = TextField('First Name',validators=[DataRequired(), Length(max=80)])
    last_name = TextField('Last Name',validators=[DataRequired(), Length(max=80)])
    age = IntegerField('Age', validators=[InputRequired()])
    sex = SelectField('Sex', choices=SEX_CHOICES,validators=[])
    # image = pass
    email = TextField('email',validators=[DataRequired(), Email(message=None), Length(min=6, max=40)])
    password = PasswordField('password',validators=[DataRequired(), Length(min=6, max=25)])
    confirm = PasswordField('Repeat password',validators=[DataRequired(),EqualTo('password', message='Passwords must match.')])

    def validate(self):
        # initial_validation = super(SignupForm,self).validate()
        # if not initial_validation:
        #     return False
        user = User.query.filter_by(email=self.email.data).first()
        if user:
            self.email.errors.append("Email already registered")
            return False
        user = User.query.filter_by(username=self.username.data).first()
        if user:
            self.username.errors.append("Username already registered")
            return False
        return True


class ChangePasswordForm(Form):
    password = PasswordField(
        'password',
        validators=[DataRequired(), Length(min=6, max=25)]
    )
    confirm = PasswordField(
        'Repeat password',
        validators=[
            DataRequired(),
            EqualTo('password', message='Passwords must match.')
        ]
    )

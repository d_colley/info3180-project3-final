## QuickStart

### Set Environment Variables

```sh
$ export APP_SETTINGS="project.config.DevelopmentConfig"
$ export APP_MAIL_USERNAME="yourgmailaccount"
$ export APP_MAIL_PASSWORD="yourgmailaccountpassword"
```

or

```sh
$ export APP_SETTINGS="project.config.ProductionConfig"
$ export APP_MAIL_USERNAME="yourgmailaccount"
$ export APP_MAIL_PASSWORD="yourgmailaccountpassword"
```

### Update Settings in Production

1. `SECRET_KEY`
1. `SQLALCHEMY_DATABASE_URI`

### Create DB

```sh
$ python manage.py create_db
$ python manage.py db init
$ python manage.py db migrate
$ python manage.py create_admin
```

### Run

```sh
$ python manage.py runserver --host=0.0.0.0
```

### Admin User Login Credentials

```text
Email: ad@min.com
Password: admin
```